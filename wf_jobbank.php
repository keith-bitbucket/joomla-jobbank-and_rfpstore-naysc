<?php

session_start();

// grab the blank template we created in Site Studio
$filename = "wf_blank.html";
$fp=fopen($filename,"rb");
$contents = fread ($fp, filesize ($filename));
fclose ($fp);

// set the page title
$pos = strpos($contents,"Blank Page for Webfire");
$contents = substr_replace($contents,"NAYSC Job Bank",$pos,22);

// print the first part of the blank template from Site Studio
// up to the rotating biz cards
//$pos1 = strpos($contents,"<!-- kef - End naysc biz card -->");
//print substr($contents, 0, $pos1 + strlen("<!-- kef - End naysc biz card -->") + 1);

// squeeze in the content
//include "bizcards/rotate.php";

// print the first part of the blank template from Site Studio
// up to the rotating biz cards
$pos = strpos($contents,"[[CONTENT GOES HERE]]");
//print substr($contents, $pos1 + strlen("<!-- kef - End naysc biz card -->") + 1, $pos -($pos1 + strlen("<!-- kef - End naysc biz card -->") + 1));
print substr($contents, 0, $pos);

// squeeze in the content
include "wf_jobbank/core.php";

// print the last part of the blank template from Site Studio
print substr($contents,$pos+strlen("[[CONTENT GOES HERE]]")+1);

?>