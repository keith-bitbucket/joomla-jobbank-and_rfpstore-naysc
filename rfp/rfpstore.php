<?
session_start();

if (isset($_SESSION['NayscRfpLastVisit']))
{
    // set cookie to current time, expire in 1 month
    setcookie("NayscRfpLastVisit",time(),time()+2592000);
}
else
{
    if (isset($_COOKIE['NayscRfpLastVisit']))
    {
        // set session and set cookie to current time
        $_SESSION['NayscRfpLastVisit'] = $_COOKIE['NayscRfpLastVisit'];
        setcookie("NayscRfpLastVisit",time(),time()+2592000);
        $_SESSION['hadcookie'] = 1;
    }
    else
    {
        // no cookie, assume first visit
        $_SESSION['NayscRfpLastVisit'] = time();
        setcookie("NayscRfpLastVisit",time(),time()+2592000);
        $_SESSION['hadcookie'] = 0;
    }
}


 if (!isset($_SESSION['is_member'])) {
   $is_member=0;
   session_register('is_member');
 }

 //------ kef -----
// $_SESSION['is_member']=1;
 //----------------

 // Connect to the database server
 $dbcnx = @mysql_connect('mysql.naysc.org', 'naysc_rfp', 'thumble');
 if (!$dbcnx) {
   die( '<p>Unable to connect to the ' .
        'database server at this time.' . mysql_error() . '</p>' );
 }

 // Select the database
 if (! @mysql_select_db('naysc_rfp') ) {
   die( '<p>Unable to locate the naysc_rfp ' .
        'database at this time.' . mysql_error() . '</p>' );
 }

 // if requesting specific rfpid, make sure access is allowed
 if ($_GET['rfpid']) {
     // Request the text of all the jokes
     $sql = "SELECT * FROM rfp WHERE id=" .$_GET['rfpid'];
     $result = @mysql_query($sql);
     if (!$result) {
       die('<p>Error performing query: ' . mysql_error() . '</p>');
     }
     $row = mysql_fetch_array($result);

     if (($row['public'] == 'N')  && (!$_SESSION['is_member'])) {
        header('Location: http://www.naysc.org/rfp/rfpstorelogin.php?rfpid='.$_GET['rfpid']);
        exit;
     }
 }

 function makeClickableLinks($text) {
   $text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)',    '<a href="\\1">LINK</a>', $text);
   $text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)',    '\\1<a href="http://\\2">LINK</a>', $text);
   $text = eregi_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})',    '<a href="mailto:\\1">\\1</a>', $text);
   $text = eregi_replace('\.">LINK', '">LINK', $text);
   return $text;
 }


 // grab the blank template we created in Site Studio
$filename = "../wf_blank.html";
$fp=fopen($filename,"rb");
$contents = fread ($fp, filesize ($filename));
fclose ($fp);

// alter all links to parent directory
$contents = str_replace("href=\"", "href=\"../", $contents);
$contents = str_replace("src=\"", "src=\"../", $contents);
$contents = str_replace("src='", "src=\"../", $contents);
$contents = str_replace("BACKGROUND=\"", "BACKGROUND=\"../", $contents);

// set the page title and print the first half of the blank template from Site Studio
$pos = strpos($contents,"Blank Page for Webfire");
$contents = substr_replace($contents,"NAYSC Consulting Opportunity Center",$pos,22);
$pos = strpos($contents,"[[CONTENT GOES HERE]]");
print substr($contents,0,$pos);

?>


<!-- kef ---------------------- START CONTENT HERE ------------------ -->


<?php
 echo '<font color=black>';
 echo "<FORM METHOD=GET ACTION=".$_SERVER['PHP_SELF']." id=myform name=myform>";
?>

<p></p>
<table align=center width=90%><tr><td align=center>
<font face="Verdana,Tahoma,Arial,Helvetica">
<font size=4>Consulting and Funding Opportunities</font></font>
</TD></TR>
</TABLE>

<?
 if ($_GET['rfpid']) {
?>
<table align=center width=90%><tr><td align=left>
<INPUT TYPE="BUTTON"  name="BACK" value="Back To List" onClick="document.forms.myform.submit();")>
</TD></TR></TABLE>
<?
     echo '<table border=0 cellpadding=3 cellspacing=1 bgcolor=gray width=90% align=center>';
     echo '<tr valign=top bgcolor=white><td>ID</td><td>'.$row['id'].'</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Created</td><td>'.$row['created'].'</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Public</td><td>'.$row['public'];
     if ($row['public'] == 'N') echo('&nbsp;<img border=0 src="images/locked.gif">');
     echo '</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Category</td><td>'.$row['category'].'</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Region</td><td>'.$row['region'].'</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Expires</td><td>'.$row['expires'].'</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Title</td><td>'.makeClickableLinks($row['title']).'</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Links</td><td>';
     $uris = preg_split("/[\s,]+/", $row['links']);
     foreach ($uris as $uri) {
         if (!strncasecmp('http',$uri,4)) {
              echo '<a href=' . $uri . '>' . $uri . '</a>';
         }
         elseif (strpos($uri,'@')) {
              echo '<a href=mailto:' . $uri . '>' . $uri . '</a>';
         }
         else {
               echo $uri;
         }
         echo '<BR>';
     }
     echo '</td></tr>';
     echo '<tr valign=top bgcolor=white><td>Item</td><td>'.nl2br($row['body']).'</td></tr>';
     echo '</table>';
 }
 else {

 // apply the search criteria
// $sql = "SELECT id,active,public,created,category,region,expires,title FROM rfp WHERE active=1 AND expires > NOW()";
 $sql = "SELECT id,active,public,UNIX_TIMESTAMP(created) as created,category,region,expires,title,(expires<CURRENT_DATE()) as expired FROM rfp WHERE active=1 AND expires > DATE_SUB(CURRENT_DATE(), INTERVAL 3 DAY)";
 if (strlen($_GET['filter'])) {
     $sql .= " AND (category = '" . $_GET['filter'] . "')";
 }
 if ($_GET['searchterm']) {
     $terms = split(" ", strtoupper($_GET['searchterm']));
     foreach ($terms as $term) {
              if (strlen($term)) {
                  $sql .= " AND (UPPER(body) like '%" . strtoupper($term) . "%')";
              }
     }
 }
 // apply the sort by
 if ($_GET['sortby_id_asc_x']) { $sortby = "ORDER BY id ASC"; }
 if ($_GET['sortby_id_desc_x']) { $sortby = "ORDER BY id DESC"; }
 if ($_GET['sortby_cat_asc_x']) { $sortby = "ORDER BY category ASC, id DESC"; }
 if ($_GET['sortby_cat_desc_x']) { $sortby = "ORDER BY category DESC, id DESC"; }
 if ($_GET['sortby_reg_asc_x']) { $sortby = "ORDER BY region ASC, id DESC"; }
 if ($_GET['sortby_reg_desc_x']) { $sortby = "ORDER BY region DESC, id DESC"; }
 if ($_GET['sortby_exp_asc_x']) { $sortby = "ORDER BY expires ASC, id DESC"; }
 if ($_GET['sortby_exp_desc_x']) { $sortby = "ORDER BY expires DESC, id DESC"; }
 if (strlen($sortby)) {
     session_register('sortby');
 } else {
     if (isset($_SESSION['sortby'])) {
         $sql .= $_SESSION['sortby'];
     } else {
          $sortby = "ORDER BY id DESC";
          session_register('sortby');
     }
 }
 $sql .= $sortby;

 //print "<br>sql=$sql<br>";
 $result = @mysql_query($sql);
 if (!$result) {
   die('<p>Error performing query: ' . mysql_error() .
       '</p>');
 }
?>

<table align=center width=90%><tr><td align=right>
<?php if ($_SESSION['hadcookie']) { echo "<font face=Arial size=2>Highlighted rows are new since your last visit.</font>"; } ?>
<?php
echo "<select name=filter>\n";
$result2 = @mysql_query("select distinct category from rfp order by category");
echo "<option value=''>Filter Select</option>\n";
while ( $row = mysql_fetch_array($result2) ) {
  echo "<option value='" . $row[0] . "'>" . $row[0] . "</option>\n";
}
echo "</select>\n";
?>
<input name=searchterm>
<input type=submit value=search>
</TD></TR></TABLE>

<?
 echo "<table border=0 cellpadding=3 cellspacing=1 width=90% align=center bgcolor=gray>";
 echo "<tr bgcolor=white>";
 echo '<td><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2><img border=0 src="images/locked.gif"></td>';
 echo '<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2><B>ID&nbsp;<INPUT TYPE="image" NAME="sortby_id_asc" SRC="../rfp/images/downarrow.gif"><INPUT TYPE="image" NAME="sortby_id_desc" SRC="../rfp/images/uparrow.gif"></td>';
 echo '<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2><B>Category&nbsp;<INPUT TYPE="image" NAME="sortby_cat_asc" SRC="../rfp/images/downarrow.gif"><INPUT TYPE="image" NAME="sortby_cat_desc" SRC="../rfp/images/uparrow.gif"></td>';
 echo '<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2><B>Region&nbsp;<INPUT TYPE="image" NAME="sortby_reg_asc" SRC="../rfp/images/downarrow.gif"><INPUT TYPE="image" NAME="sortby_reg_desc" SRC="../rfp/images/uparrow.gif"></td>';
 echo '<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2><B>Expires&nbsp;<INPUT TYPE="image" NAME="sortby_exp_asc" SRC="../rfp/images/downarrow.gif"><INPUT TYPE="image" NAME="sortby_exp_desc" SRC="../rfp/images/uparrow.gif"></td>';
 echo '<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2><B>Title</td>';
 echo"</tr>";

 $color_toggle=1;
 while ( $row = mysql_fetch_array($result) ) {
   $color_toggle = 1 - $color_toggle;
   echo "<tr valign=top bgcolor=";
   if ($_SESSION['NayscRfpLastVisit'] < $row['created']) { echo('#ffffcc'); }
   else { echo('#e3e3e3'); }
   echo "><td align=center>";
   if ($row['public'] == 'N') { echo('<img border=0 src="images/locked.gif">'); }
   echo '</td>';
   echo('<td align=center><font face="Verdana,Tahoma,Arial,Helvetica" size=2>');

   if (!$_SESSION['is_member'] && $row['public']=='N') {
//     echo('<a target=_blank href=http://www.naysc.org/rfp/rfpstorelogin.php?rfpid='. $row['id'] .'><font face="Verdana,Tahoma,Arial,Helvetica">' . $row['id'] . '</a>');
     echo('<a href=http://www.naysc.org/rfp/rfpstorelogin.php?rfpid='. $row['id'] .'><font face="Verdana,Tahoma,Arial,Helvetica">' . $row['id'] . '</a>');
   }
   else {
//     echo('<a target=_blank href='.$_SERVER['PHP_SELF'].'?rfpid='. $row['id'] .'>' . $row['id'] . '</a>');
     echo('<a href='.$_SERVER['PHP_SELF'].'?rfpid='. $row['id'] .'>' . $row['id'] . '</a>');
   }
   echo ('</td>');
   echo('<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2>' . $row['category'] . '</td>');
   echo('<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2>' . $row['region'] . '</td>');
   if ($row['expires'] == '2050-01-01') { $row['expires']='N/A'; }
   echo('<td nowrap><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2');
   if ($row['expired']) { echo(' color=gray> <strike'); }
   echo('>' . $row['expires'] . '</font></td>');
   echo('<td><font face=\"Verdana,Tahoma,Arial,Helvetica\" size=2>' . makeClickableLinks($row['title']) . '</td>');
   echo "</tr>";
 }
 echo "</table>";
 }
 echo "</FORM>";
?>

<font face="Verdana,Tahoma,Arial,Helvetica" size=1>
Disclaimer: NAYSC is not responsible for assuring the accuracy of the opportunities listed on this website.
Please contact the purchasing agent or contact person to verify due dates and information.
Due to limited space, the full announcement may not appear and it is the responsibility of the user of this
listing to visit the link to the full announcement or check with the appropriate funding or
purchasing agent for complete details.</font><P>
<!-- kef ---------------------- STOP CONTENT HERE ------------------ -->

<?php
// print the last half of the blank template from Site Studio
print substr($contents,$pos+21);
?>