<center>
<font face="Arial,Tahoma,Verdana,Helvetica" size=2>
<font size='+1' color='#000000'>EMPLOYMENT AND CONSULTING MESSAGE BOARD</font>

<p><div align=left>
Any non-profit, for-profit, government or tribal entity, or educational
organization can post a job or consulting notice at no cost. Please keep
in mind that our members and subscribers are professional consultants
and experts. Part-time, full-time, temporary, contractual, and
consulting jobs are all acceptable. Volunteer listings are not accepted.
Inappropriate listings or unsolicited marketing to our members will be
immediately deleted from the site.
</div></p>
<p>
<form method=POST action=wf_jobbank.php>

<?php

$admin_email = "info@naysc.org";

if (!isset($_SESSION['is_member'])) {
    $_SESSION['is_member']=0;
}

if (get_magic_quotes_gpc()) {
 // Yes? Strip the added slashes
 $_REQUEST = array_map('stripslashes', $_REQUEST);
 $_GET = array_map('stripslashes', $_GET);
 $_POST = array_map('stripslashes', $_POST);
 $_COOKIE = array_map('stripslashes', $_COOKIE);
}


if (isset($_POST['submit']))
{
    if ($_POST['submit'] == "Cancel")
    {
        $_SESSION['action']="SHOWJOBS";
    }
    else if (($_POST['submit'] == "Add") && ($_SESSION['action']=="SHOWADDJOB"))
    {
        $_SESSION['action']="ADDJOB";
    }
    else if (($_POST['submit'] == "Update") && ($_SESSION['action']=="SHOWUPDATEJOB"))
    {
        $_SESSION['action']="UPDATEJOB";
    }
    else if (($_POST['submit'] == "Update") && ($_SESSION['action']=="SHOWJOB"))
    {
        $_SESSION['action']="SHOWUPDATEJOB";
    }
    else if (($_POST['submit'] == "Login") && ($_SESSION['action']=="SHOWLOGIN"))
    {
        $_SESSION['action']="LOGIN";
    }
    else
    {
        $_SESSION['action']="SHOWJOBS";
    }
}
else if (isset($_GET['action']))
{
    $_SESSION['action']=$_GET['action'];
}
else
{
    $_SESSION['action']="SHOWJOBS";
}

// will not need db for SHOWADDJOB
if ($_SESSION['action'] != "SHOWADDJOB")
{
    $dbcnx = @mysql_connect('mysql.naysc.org', 'naysc_rfp', 'thumble');
    if (!$dbcnx) {
         die( '<p>Unable to connect to db at this time.' . mysql_error() . '</p>' );
    }
    if (! @mysql_select_db('naysc_rfp') ) {
         die( '<p>Unable to select db at this time.' . mysql_error() . '</p>' );
    }
}

/*
**
**        ADDJOB
**
*/

if ($_SESSION['action']=="ADDJOB") {
    $sql = "insert into jobs (email,password,title,expiration,description,company,contact,phone,comments,web) values ('" . mysql_escape_string($_POST['email']) . "','" . mysql_escape_string($_POST['password']) . "','" . mysql_escape_string($_POST['title']) . "','" . $_POST['expiration'] . "','". mysql_escape_string($_POST['description']) . "','" . mysql_escape_string($_POST['company']) . "','" . mysql_escape_string($_POST['contact']) . "','" . $_POST['phone'] . "','". mysql_escape_string($_POST['comments']) . "','" . mysql_escape_string($_POST['web']) . "')";
    $result = @mysql_query($sql);
    if (!$result) {
       die('<p>Error performing query: ' . mysql_error() . '</p>');
    }
    echo "<p>Job added successfully.  Remember the password so you can make changes in the future.</p>\n";
    //email_job("A new job was created.", "New NAYSC Job Bank Posting", mysql_insert_id(), $admin_email);
}

/*
**
**        UPDATEJOB
**
*/

else if ($_SESSION['action']=="UPDATEJOB") {
    $sql = "update jobs set "
         . " company='" . mysql_escape_string($_POST['company']) . "'"
         . ",contact='" . mysql_escape_string($_POST['contact']) . "'"
         . ",phone='" . mysql_escape_string($_POST['phone']) . "'"
         . ",email='" . mysql_escape_string($_POST['email']) . "'"
         . ",web='" . mysql_escape_string($_POST['web']) . "'"
         . ",password='" . mysql_escape_string($_POST['password']) . "'"
         . ",expiration='" . mysql_escape_string($_POST['expiration']) . "'"
         . ",title='" . mysql_escape_string($_POST['title']) . "'"
         . ",comments='" . mysql_escape_string($_POST['comments']) . "'"
         . ",description='" . mysql_escape_string($_POST['description']) . "'"
         . " where id=" . $_POST['jobid'];
    $result = @mysql_query($sql);
    if (!$result) {
       die('<p>Error performing query: ' . mysql_error() . '</p>');
    }
    echo "<p>Job updated successfully.  Remember the password so you can make changes in the future.</p>\n";
    //email_job("A job was updated.", "Updated NAYSC Job Bank Posting", $_POST['jobid'], $admin_email);
}

/*
**
**        SHOWJOBS
**
*/

else if ($_SESSION['action']=="SHOWJOBS")
{
     $sql = "select id, title, expiration, web from jobs where expiration >= now() order by id DESC";
     $result = mysql_query($sql);
     echo "<table cellspacing=0 cellpadding=3 border=1>";
     while ( $row = mysql_fetch_array($result) )
     {
         echo "<tr>";
         echo "<td>" . $row['id'] . "</td>\n";
         echo "<td>" . $row['title'] . "<br />";
         echo "<i>Expires: " . $row['expiration'] . "</i>";
         if (strlen($row['web'])) { echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<a href=http://" . str_replace('http://','',$row['web']) . ">" . str_replace('http://','',$row['web']) . "</a>)"; }
         echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
         if ($_SESSION['is_member'])
         {
            echo "<a href=wf_jobbank.php?action=SHOWJOB&jobid=" . $row['id'] . ">View Details</a>";
         }
         else
         {
            echo "<a href=wf_jobbank.php?action=SHOWLOGIN&jobid=" . $row['id'] . ">Login To View Details</a>";
         }
         echo "</td>\n";
         echo "</tr>\n";
     }
     echo "</table>\n";
}

/*
**
**        SHOWJOB
**
*/

else if ($_SESSION['action']=="SHOWJOB" && $_SESSION['is_member'] == 1)
{
     $sql = "select * from jobs where id=" . $_GET['jobid'];
     $result = mysql_query($sql);
     echo "<table cellspacing=0 cellpadding=3 border=1>";
     $row = mysql_fetch_array($result);
     echo "<tr><td>Company:</td><td>" . $row['company'] . "</td></tr>\n";
     echo "<tr><td>Contact:</td><td>" . $row['contact'] . "</td></tr>\n";
     echo "<tr><td>Phone:</td><td>" . $row['phone'] . "</td></tr>\n";
     echo "<tr><td>Email:</td><td>" . $row['email'] . "</td></tr>\n";
     echo "<tr><td>Web:</td><td>" . $row['web'] . "</td></tr>\n";
     echo "<tr><td>Expiration:</td><td>" . $row['expiration'] . "</td></tr>\n";
     echo "<tr><td>Title:</td><td>" . $row['title'] . "</td></tr>\n";
     echo "<tr><td>Employer Comments:</td><td>" . $row['comments'] . "</td></tr>\n";
     echo "<tr><td>Description:</td><td>" . nl2br($row['description']) . "</td></tr>\n";
     echo "</table>\n";

     echo "<input type=hidden name=jobid value=" . $row['id'] . ">";
     echo "<p>To update, enter password then click Upate button?<br />Password:  <input type=text size=30 name=password>  <input type=submit name=submit value=\"Update\">";
     echo "<br />(<a href=wf_jobbank.php?action=FORGOTPASSWORD&jobid=" . $row['id'] . ">Forgot Password?</a>)</p>\n";
}

/*
**
**        FORGOTPASSWORD
**
*/

else if ($_SESSION['action']=="FORGOTPASSWORD")
{
     $sql = "select contact,email from jobs where id=" . $_GET['jobid'];
     $result = mysql_query($sql);
     $row = mysql_fetch_array($result);
     if (strlen($row['email']) > 6)
     {
          //email_job("A job bank password request was issued for the following.", "NAYSC Job Bank Password Request", $_GET['jobid'], $row['email']);
     }
     else
     {
          //email_job("A job bank password request was issued for the following, however they do not have a valid email address.", "NAYSC Job Bank Password Request", $_GET['jobid'], $admin_email);
     }
}

/*
**
**        SHOWLOGIN
**
*/

else if ($_SESSION['action']=="SHOWLOGIN")
{
    echo "<input type=hidden name=jobid value=" . $_GET['jobid'] . ">";
    ?>
    <div align=left>
    <h3>Member and Subscriber Login:</h3>
    To view the details of this item you must first login with your NAYSC account.  If you do not
    have an account then use the links <a href="http://www.naysc.org/">here</a> to join now.
    <p>
    To return without logging in, <a href=http://www.naysc.org/wf_jobbank.php>click here</a>.
    </p>

    <p>
    Username:  <INPUT TYPE="TEXT"  name="username"><BR>
    Password:  <INPUT TYPE="PASSWORD"  name="password"><BR>
    <INPUT TYPE="SUBMIT" name="submit" value="Login">
    </p>
    </div>
    <?php
}

/*
**
**        LOGIN
**
*/

else if ($_SESSION['action']=="LOGIN")
{
    if (isset($_POST['username']) && isset($_POST['password']))
    {
        $xml_url = "http://web.memberclicks.com/mc/common/validateUser.do?OrgId=naysc&txtUserId=".$_POST['username']."&txtPassword=".$_POST['password'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$xml_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result=curl_exec ($ch);
        curl_close ($ch);
        if (strpos($result, "<valid>1</valid>") === false) {
          $is_member=0;
        } else {
          $is_member=1;
        }

/*
        $lines = file ($xml_url);
        foreach ($lines as $line_num => $line)
        {
            if (!strncmp($line,"<valid>1</valid>",16))
            {
                 $_SESSION['is_member'] = 1;
                 break;
            }
        }
*/
        $_SESSION['is_member'] = $is_member;
        if ($_SESSION['is_member'] == 1)
        {
            echo "<p>Successful login.</p>\n";
            echo "<p>Click <a href=http://www.naysc.org/wf_jobbank.php?action=SHOWJOB&jobid=".$_POST['jobid'].">here</a> to return to NAYSC Job Bank.</p>\n";
        }
        else
        {
            echo "<p>Login Failed!</p>\n";
            echo "<p>Click <a href=wf_jobbank.php?action=SHOWJOBS>here</a> to return to NAYSC Job Bank.</p>\n";
        }
    }
    else
    {
        echo "<p>Login Failed!</p>\n";
        echo "<p>Click <a href=wf_jobbank.php?action=SHOWJOBS>here</a> to return to NAYSC Job Bank.</p>\n";
    }
}

?>
</center>
<?php

/*
**
**        SHOWUPDATEJOB || SHOWADDJOB
**
*/

if ($_SESSION['action']=="SHOWUPDATEJOB" || $_SESSION['action']=="SHOWADDJOB")
{

    $doupdate=0;
    $badpassword=0;
    if ($_SESSION['action']=="SHOWUPDATEJOB")
    {
         $sql = "select * from jobs where id=" . $_POST['jobid'];
         $result = mysql_query($sql);
         $row = mysql_fetch_array($result);
         if (($_POST['password'] == 'dobbertales') || ($row['password']==$_POST['password']))
         {
            $doupdate=1;
         }
         else
         {
            $badpassword=1;
            echo "<p><font color=red>Password incorrect!</font></p>";
         }
    }

    if (!$badpassword) {
?>

<h3>Employer Information</h3>
<table cellpadding=2 cellspacing=0 border=0>
<tr>
    <td>Company/Business Name:</td>
    <td><input name=company maxlength=250 size=50 type=text <? if ($doupdate) { echo "value=\"" . $row['company'] . "\"";} ?> /></td>
</tr>
<tr>
    <td>Contact Name:</td>
    <td><input name=contact maxlength=250 size=50 type=text <? if ($doupdate) { echo "value=\"" . $row['contact'] . "\"";} ?> /></td>
</tr>
<tr>
    <td>Phone Number:</td>
    <td><input name=phone maxlength=50 size=50 type=text <? if ($doupdate) { echo "value=\"" . $row['phone'] . "\"";} ?> /></td>
</tr>
<tr>
    <td>Email Address:</td>
    <td><input name=email size=50 maxlength=250 type=text <? if ($doupdate) { echo "value=\"" . $row['email'] . "\"";} ?> /></td>
</tr>
<tr>
    <td>Web Address:</td>
    <td><input name=web size=50 maxlength=250 type=text <? if ($doupdate) { echo "value=\"" . $row['web'] . "\"";} ?> /></td>
</tr>
</table>

<h3>Job Posting</h3>
<table cellpadding=2 cellspacing=0 border=0>
<tr>
    <td>Password (so you can make changes):</td>
    <td><input name=password maxlength=50 size=50 type=text <? if ($doupdate) { echo "value=\"" . $row['password'] . "\"";} ?> /></td>
</tr>
<tr>
    <td>Expiration Date (YYYY-MM-DD):</td>
    <td><input name=expiration size=50 type=text <? if ($doupdate) { echo "value=\"" . $row['expiration'] . "\"";} ?> /><br />
    <? if ($doupdate) { echo "<font color=blue>To remove this opportunity, set expiration to 2000-01-01.</font>"; } ?></td>
</tr>
<tr>
    <td>Job Title:</td>
    <td><input name=title size=50 maxlength=250 type=text <? if ($doupdate) { echo "value=\"" . $row['title'] . "\"";} ?> /></td>
</tr>
<tr>
    <td>Employer Comments:</td>
    <td><input name=comments size=50 MAXLENGTH=250 type=text <? if ($doupdate) { echo "value=\"" . $row['comments'] . "\"";} ?> /></td>
</tr>
<tr>
    <td>Job Description:</td>
    <td><textarea name=description cols=50 rows=20><? if ($doupdate) { echo $row['description']; } ?></textarea></td>
</tr>
</table>

<?php if ($_SESSION['action']=="SHOWADDJOB") { ?>
    <input type=submit name=submit value="Add">
<?php } else { ?>
    <input type=hidden name=jobid value=<? echo $row['id']; ?>>
    <input type=submit name=submit value="Update">
<?php } ?>

<input type=submit name=submit value="Cancel">
<input type=reset name=reset value="Clear">
<?php  }  ?>
<?php }   ?>


<center>
<p>
<?php if ($_SESSION['action']!="LOGIN") { ?>
|<a href="wf_jobbank.php?action=SHOWJOBS">Show Employment Opportunities</a>|
&nbsp;&nbsp;
|<a href="wf_jobbank.php?action=SHOWADDJOB">Add Employment Opportunity</a>|
<?php } ?>
</p>
</form>
</p>
</font>
</center>



<?php
function email_job($msg, $subject, $jobid, $address)
{
    $dbcnx = @mysql_connect('mysql.naysc.org', 'naysc_rfp', 'thumble');
    @mysql_select_db('naysc_rfp');
    $sql = "select * from jobs where id=" . $jobid;
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    $out = $msg . "\r\n\r\n"
         . "Job ID:   " . $jobid . "\r\n"
         . "Company:  " . $row['company'] . "\r\n"
         . "Contact:  " . $row['contact'] . "\r\n"
         . "Phone:    " . $row['phone'] . "\r\n"
         . "Email:    " . $row['email'] . "\r\n"
         . "Web:      " . $row['web'] . "\r\n"
         . "Password: " . $row['password'] . "\r\n"
         . "Expires:  " . $row['expiration'] . "\r\n"
         . "Title:    " . $row['title'] . "\r\n"
         . "Comments: " . $row['comments'] . "\r\n"
         . "Description:\r\n\r\n" . $row['description'] . "\r\n";
    mail($address,$subject,$out,"From: ".$admin_email);
}