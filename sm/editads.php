<HTML>
<HEAD>


<META NAME="description" CONTENT="National Association of Youth Service Consultants  (NAYSC)
">
<META NAME="Keywords"    CONTENT="National, Association, youth, service, consultants,technical assistance, TA, provider, education, mental health, foster care, youth development, independent living, juvenile, justice, court-involved youth, youth employment, employment and training"> 



<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<TITLE>National Association of Youth Service Consultants </TITLE></head>
<BODY  BGCOLOR="#2F4F88"
 TEXT="#CCCCCC"

MARGINHEIGHT="0" MARGINWIDTH="0" LEFTMARGIN="0" TOPMARGIN="0" RIGHTMARGIN="0" BOTTOMMARGIN="0"

 onLoad=""
 LINK="#FFFFFF"
 VLINK="#FFFF99"
 ALINK="#FFFFFF"
>

  
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 width="100%" >
	<TR>
		<TD WIDTH=187 HEIGHT=1>
			<IMG SRC="../../widgets/spacer.gif" WIDTH=187 HEIGHT=1></TD>
		<TD WIDTH=5 HEIGHT=1>
			<IMG SRC="../../widgets/spacer.gif" WIDTH=5 HEIGHT=1></TD>
		<TD WIDTH=372 HEIGHT=1>
			<IMG SRC="../../widgets/spacer.gif" WIDTH=372 HEIGHT=1></TD>
		<TD WIDTH=111 HEIGHT=1>
			<IMG SRC="../../widgets/spacer.gif" WIDTH=111 HEIGHT=1></TD>
		<TD WIDTH=5 HEIGHT=1>
			<IMG SRC="../../widgets/spacer.gif" WIDTH=5 HEIGHT=1></TD>
		<TD WIDTH=120 HEIGHT=1>
			<IMG SRC="../../widgets/spacer.gif" WIDTH=120 HEIGHT=1></TD>
	</TR>
	<TR>
		<TD WIDTH=564 HEIGHT=102 COLSPAN=3 align="left" valign="top" bgcolor="#011343">
			<IMG SRC="../../widgets/gen_319.1.gif" WIDTH=564 HEIGHT=102></TD>
		<td width="116" height="122" colspan="2" rowspan="2" background="../../widgets/gen_289.1.gif">
			<IMG SRC="../../widgets/spacer.gif" WIDTH="1" HEIGHT="1" border="0" alt=""></td>
		<td width="100%" height="122" rowspan="2" align="right" valign="top" background="../../widgets/gen_289.1.gif">
			<IMG SRC="../../widgets/spacer.gif" WIDTH="1" HEIGHT="1" border="0" alt=""></td>
	</TR>
	<TR>
		<td width="564" height="20" colspan="3" BACKGROUND="../../widgets/gen_285.1.gif"><IMG SRC="../../widgets/gen_334.1.gif" WIDTH=564 HEIGHT=20></td>
	</TR>

</TABLE>


<!-- ---------------------- START CONTENT HERE ------------------ -->

<style type="text/css">
    table { border: #011343 1px solid; border-collapse: collapse; border-spacing: 0px; width: 100%; }
    th    { border: #011343 1px solid; padding: 4px; background: #add8e6; }
    td    { border: #011343 1px solid; padding: 3px; }
    hr    { border: 0px solid; padding: 0px; margin: 0px; border-top-width: 1px; height: 1px; }
</style>

<?php

/*
 * IMPORTANT NOTE: This generated file contains only a subset of huge amount
 * of options that can be used with phpMyEdit. To get information about all
 * features offered by phpMyEdit, check official documentation. It is available
 * online and also for download on phpMyEdit project management page:
 *
 * http://www.platon.sk/projects/main_page.php?project_id=5
 */

// MySQL host name, user name, password, database, and table
$opts['hn'] = 'localhost';
$opts['un'] = 'smaxwell';
$opts['pw'] = 'sm12345';
$opts['db'] = 'smaxwell';
$opts['tb'] = 'textads';

// Name of field which is the unique key
$opts['key'] = 'id';

// Type of key field (int/real/string/date etc.)
$opts['key_type'] = 'int';

// Sorting field(s)
$opts['sort_field'] = array('id');

// Number of records to display on the screen
// Value of -1 lists all records in a table
$opts['inc'] = 15;

// Options you wish to give the users
// A - add,  C - change, P - copy, V - view, D - delete,
// F - filter, I - initial sort suppressed
$opts['options'] = 'ACPVDF';

// Number of lines to display on multiple selection filters
$opts['multiple'] = '4';

// Navigation style: B - buttons (default), T - text links, G - graphic links
// Buttons position: U - up, D - down (default)
$opts['navigation'] = 'GU';

// Display special page elements
$opts['display'] = array(
	'form'  => true,
	'query' => true,
	'sort'  => true,
	'time'  => true,
	'tabs'  => true
);

/* Get the user's default language and use it if possible or you can
   specify particular one you want to use. Refer to official documentation
   for list of available languages. */
$opts['language'] = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

/* Table-level filter capability. If set, it is included in the WHERE clause
   of any generated SELECT statement in SQL query. This gives you ability to
   work only with subset of data from table.

$opts['filters'] = "column1 like '%11%' AND column2<17";
$opts['filters'] = "section_id = 9";
$opts['filters'] = "PMEtable0.sessions_count > 200";
*/

/* Field definitions
   
Fields will be displayed left to right on the screen in the order in which they
appear in generated list. Here are some most used field options documented.

['name'] is the title used for column headings, etc.;
['maxlen'] maximum length to display add/edit/search input boxes
['trimlen'] maximum length of string content to display in row listing
['width'] is an optional display width specification for the column
          e.g.  ['width'] = '100px';
['mask'] a string that is used by sprintf() to format field output
['sort'] true or false; means the users may sort the display on this column
['strip_tags'] true or false; whether to strip tags from content
['nowrap'] true or false; whether this field should get a NOWRAP
['required'] true or false; if generate javascript to prevent null entries
['select'] T - text, N - numeric, D - drop-down, M - multiple selection
['options'] optional parameter to control whether a field is displayed
  L - list, F - filter, A - add, C - change, P - copy, D - delete, V - view
            Another flags are:
            R - indicates that a field is read only
            W - indicates that a field is a password field
            H - indicates that a field is to be hidden and marked as hidden
['URL'] is used to make a field 'clickable' in the display
        e.g.: 'mailto:$value', 'http://$value' or '$page?stuff';
['URLtarget']  HTML target link specification (for example: _blank)
['textarea']['rows'] and/or ['textarea']['cols']
  specifies a textarea is to be used to give multi-line input
  e.g. ['textarea']['rows'] = 5; ['textarea']['cols'] = 10
['values'] restricts user input to the specified constants,
           e.g. ['values'] = array('A','B','C') or ['values'] = range(1,99)
['values']['table'] and ['values']['column'] restricts user input
  to the values found in the specified column of another table
['values']['description'] = 'desc_column'
  The optional ['values']['description'] field allows the value(s) displayed
  to the user to be different to those in the ['values']['column'] field.
  This is useful for giving more meaning to column values. Multiple
  descriptions fields are also possible. Check documentation for this.
*/

$opts['fdd']['id'] = array(
  'name'     => 'ID',
  'select'   => 'T',
  'options'  => 'AVCPDR', // auto increment
  'maxlen'   => 11,
  'default'  => '0',
  'sort'     => true
);
$opts['fdd']['title'] = array(
  'name'     => 'Title',
  'select'   => 'T',
  'maxlen'   => 255,
  'required' => true,
  'sort'     => true
);
$opts['fdd']['body'] = array(
  'name'     => 'Body',
  'select'   => 'T',
  'maxlen'   => 16777215,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'required' => true,
  'sort'     => true
);
$opts['fdd']['active'] = array(
  'name'     => 'Active',
  'select'   => 'T',
  'maxlen'   => 1,
  'values'   => array(
                  "Y",
                  "N"),
  'default'  => 'Y',
  'required' => true,
  'sort'     => true
);
$opts['fdd']['disporder'] = array(
  'name'     => 'Disporder',
  'select'   => 'T',
  'maxlen'   => 11,
  'default'  => '999',
  'required' => true,
  'sort'     => true
);

// Now important call to phpMyEdit
require_once 'phpMyEdit.class.php';
new phpMyEdit($opts);

?>

<!-- ---------------------- STOP CONTENT HERE ------------------ -->

<FONT FACE="verdana, arial, helvetica" SIZE="4">
<center>
<a href="http://www.naysc.org/sm/publishads.php">Publish Ads</A><BR>
<p><font size=1><a href="http://www.naysc.org/">HOME</A></font>
</center>
</font>


</body>
</html>