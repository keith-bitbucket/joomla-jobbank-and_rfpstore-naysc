<?php

function showitem ($hideIfEmpty, $item, $prefix, $suffix)
{
    if (($item!='') || (!$hideIfEmpty))
    {
        return ($prefix . $item . $suffix);
    }
}


// map export csv column names
$handle = fopen ("naysc-export.csv","r");
$col = fgetcsv ($handle, 16384, ",");
$ncol = count ($col);
for ($c=0; $c<$ncol; $c++) {
     $col[$c] = strtolower($col[$c]);
     print $col[$c] . '<BR>';
}

// throw away the blank line
fgets ($handle, 16384);

// build html files from profiles
$numprofiles = 0;
while ($data = fgetcsv ($handle, 16384, ",")) {
    $num = count ($data);
    if ($num == $ncol) {
        ++$numprofiles;
        print "$numprofiles<br>\n";
        $fname = "l37/" . $numprofiles . ".inc";
        $fout = fopen ($fname, "w");
        for ($c=0; $c<$ncol; $c++) {
             $arr[$col[$c]] = $data[$c];
        }
// clean up formatting of keywords
$arr['keywords'] = str_replace (",", ", ", $arr['keywords']);
$arr['keywords'] = str_replace ("\r", "", $arr['keywords']);

// print the profile
fputs($fout, "
&nbsp;<br>
<table bgcolor=#8a8a8a border=0 cellspacing=0 cellpadding=2 width=225><tr><td>

<table border=0 cellpadding=3 cellspacing=0 width=221><tr><td bgcolor=#e3e3e3>
<center><font face=Arial size=3 color=#6666cc>
<b>Meet NAYSC Member...</b></font><br><font color=black>");
//fputs ($fout, showitem (1,$arr['mc_id'],"<a href=http://mcweb01.memberclicks.com/mc/login.do?hidOrgID=naysc&txtUserId=guest&txtPassword=guest&isencodedstring=no&mcevent=login&destination=ViewProfile&userID=","><font face=Arial size=1>(view profile)</font></a><BR>"));
fputs ($fout, showitem (1,$arr['photo'],"<IMG SRC=http://web.memberclicks.com/mcdatafiles/images/profilephoto/naysc/"," WIDTH=150><br>\n"));
fputs ($fout, "</center>"
. showitem (1,$arr['company name'],"<font face=Arial size=2><b>","</b><br></font>")
. "<font face=Arial size=1>"
. $arr['first name']." ".$arr['last name']."</b><br>"
. showitem (1,$arr['address 1'],"","<br>")
. showitem (1,$arr['address 2'],"","<br>")
. showitem (1,$arr['city'],"",", ")
. showitem (1,$arr['state of residence'],""," ")
. $arr['zip']."<br>"
. showitem (1,$arr['phone'],"Phone: ","<br>")
. showitem (1,$arr['fax'],"Fax: ","<br>")
. showitem (1,$arr['email'],"<A HREF=mailto:",">")
. showitem (1,$arr['email'],"","</A><br>")
. showitem (1,$arr['website'],"<A TARGET=_blank HREF=",">")
. showitem (1,$arr['website'],"","</A>")
. showitem (1,$arr['keywords'],"<P>Keywords: ","")
. "</font></font></td></tr></table>\n</td></tr></table>\n");
        fclose ($fout);
    }
}
fclose ($handle);

print "$numprofiles processed.<BR>";
chmod ("../layout27.cnt",666);
$handle = fopen ("../layout27.cnt","r+");
$s = fgets ($handle,1024);
$v = explode(" ", $s);
print "read $v[0] $v[1]<BR>\n";
$v[0] = $numprofiles;
$s = implode (" ", $v);
print "wrote $s<BR>\n";
rewind ($handle);
fputs ($handle, $s);
fclose ($handle);
?>



<!--
/*
  ##First Name## ##Last Name##

##Company Name##

##Address 1##

##Address 2##

##City##
, ##State of Residence##
##Zip##

##null##
##null##

Phone: ##Phone##
##null##

Fax: ##Fax##

##Email##

##Website##
  ##Photo##
*/
-->