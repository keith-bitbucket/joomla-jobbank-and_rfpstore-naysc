<?php

/**
* wp-mail.php
*
* Copyright (c) 2003-2004 The Wordpress Team
* Copyright (c) 2004 - John B. Hewitt - jb@stcpl.com.au
* Copyright (c) 2004 - Dan Cech - dcech@lansmash.com
*
* Licensed under the GNU GPL. For full terms see the file COPYING.
*
* History
*
* Version 0.2
* - Stopped using pear body decoding in favour of own decoding (may be slower but more modifiable) because of enriched text decoding
* - Added base64_decode checking (may help mobile phone users)
* - Fixed Subject line for non-english users (htmlentities instead of just trim)
* - Fixed error in some pop hanging -> more graceful exit on event on no emails in inbox ($pop3->quit)
* - Added work around for email addresses with exta <> in field (ie: <blade@lansmash.com> instead of blade@lasmash.com
* - Added some ===basic=== enriched text support
* - Updated readme file for easier install
* - Easy modify of globals (such as photosdir and filesdir)
* - Cleaned up some pear stuff in install
*
* Version 0.1
* First release
**/

	//global vars you can change
	$photosdir = 'wp-photos/';
	$filesdir = 'wp-filez/';

//	require(dirname(__FILE__) . '/wp-config.php');
	require_once('class-pop3.php');
	require_once ('mimedecode.php');

	$time_difference = get_settings('gmt_offset');

	//retrieve mail
	$pop3 = new POP3();
	if (!$pop3->connect('www.naysc.org','110')) :
	echo "Ooops $pop3->ERROR <br />\n";
	exit;
	endif;

	$count = $pop3->login('dbupdater','thumble');
	if (0 == $count)
	{
		$pop3->quit();
		die(__('There does not seem to be any new mail.'));
	}


	//loop through messages
	for ($i=1; $i <= $count; $i++)
	{
		//variables
		$content_type = '';
		$boundary = '';
		$bodysignal = 0;

		$input = implode ('',$pop3->get($i));
//		$input = file_get_contents('./email.txt');

		if(!$pop3->delete($i))
		{
			echo '<p>Oops '.$pop3->ERROR.'</p></div>';
			$pop3->reset();
			exit;
		} else {
			echo "<p>Mission complete, message <strong>$i</strong> deleted.</p>";
		}

		//decode the mime
		$params['include_bodies'] = true;
		$params['decode_bodies'] = false;
		$params['decode_headers'] = true;
		$params['input'] = $input;
		$structure = Mail_mimeDecode::decode($params);
		//print_r ($structure);
		$subject = htmlentities($structure->headers['subject']);
		$ddate = trim($structure->headers['date']);
		$from = trim($structure->headers['from']);
		//work around for users with extra <> in email address
		if (preg_match('/^[^<>]+<([^<>]+)>$/',$from,$matches))
		{
			$from = $matches[1];
		}
		$content = get_content($structure);

		//date reformating
        /* kef - swapped the following two values */
		$post_date = date('Y-m-d H:i:s', time($ddate));  //kef + ($time_difference * 3600));
		$post_date_gmt = gmdate('Y-m-d H:i:s', time($ddate) );

		//remove signature
		$content = removesig($content);
		//filter new lines
		$content = filternewlines($content);

		//try and determine category
		if ( preg_match('/.*\[(.+)\](.+)/', $subject, $matches) )
		{
			$post_categories[0] = $matches[1];
			$subject = $matches[2];
		}

		if (empty($post_categories))
		$post_categories[] = get_settings('default_category');

		//report
		// print '<p><b>Mail Format</b>: ' . $mailformat . '</p>' . "\n";
		print '<p><b>From</b>: ' . $from . '<br />' . "\n";
		print '<b>Date</b>: ' . $post_date . '<br />' . "\n";
		print '<b>Date GMT</b>: ' . $post_date_gmt . '<br />' . "\n";
		print '<b>Category</b>: ' . $post_categories[0] . '<br />' . "\n";
        print '<b>Subject</b>: ' . $subject . '<br />' . "\n";
        print '<b>Time Diff</b>: ' . $time_difference . '<br />' . "\n";
		print '<b>Posted content:</b></p><hr />' . $content . '<hr />';

		//see if sender is a valid sender from the wp database
        /* kef - it really is me doing the posting */
		if ($from = "cell@keithford.com") {
			$from = "keith@keithford.com";
		}
		$sql = 'SELECT id FROM '.$tableusers.' WHERE user_email=\'' . addslashes($from) . '\'';
		if (!$poster = $wpdb->get_var($sql))
		{
			echo '<h1>Invalid sender: ' . htmlentities($from) . " !</h1><h2>Not adding email!</h2><br />\n";
			continue;
		}

		$details = array(
			'post_author'		=> $poster,
			'post_date'			=> $post_date,
			'post_date_gmt'		=> $post_date_gmt,
			'post_content'		=> $content,
			'post_title'		=> $subject,
			'post_modified'		=> $post_date,
			'post_modified_gmt'	=> $post_date_gmt
		);

		//generate sql
		$sql = 'INSERT INTO '.$tableposts.' ('. implode(',',array_keys($details)) .') VALUES (\''. implode('\',\'',array_map('addslashes',$details)) . '\')';

		$result = $wpdb->query($sql);
		$post_ID = $wpdb->insert_id;

		do_action('publish_post', $post_ID);
		do_action('publish_phone', $post_ID);
		pingback($content, $post_ID);

		foreach ($post_categories as $post_category)
		{
			$post_category = intval($post_category);

			// Double check it's not there already
			$exists = $wpdb->get_row("SELECT * FROM $tablepost2cat WHERE post_id = $post_ID AND category_id = $post_category");

			if (!$exists && $result) { 
				$wpdb->query("
				INSERT INTO $tablepost2cat
				(post_id, category_id)
				VALUES
				($post_ID, $post_category)
				");
			}
		}
	} // end looping over messages

	$pop3->quit();

/** FUNCTIONS **/

	//tear apart the meta part for useful information
	function get_content ($part) 
	{
		global $photosdir,$filesdir;
		//fixes
		$part = charsetcheck($part);
		$part = base64check($part);
		switch ( strtolower($part->ctype_primary) )
		{
			case 'multipart':
				$meta_return = '';
				foreach ($part->parts as $section)
				{
					$meta_return .= get_content($section);
				}
				break;
			case 'text':
				//dump the enriched stuff
				if ($part->ctype_secondary=='enriched') 
				{
					$meta_return = etf2html($part->body ) . "\n";
				} else
				{
					$meta_return = htmlentities( $part->body ) . "\n";
				}
				break;

			case 'image':
				$filename = $photosdir . rand() . '.' . $part->ctype_secondary;
				$fp = fopen($filename, 'w');
				fwrite($fp, $part->body);
				fclose($fp);
                /* kef - set width to 320, need to have a thumbnail generator and link instead */
				$meta_return .= '<img width="320" src="' . $filename . '" alt="' . $part->ctype_parameters['name'] . '"/>' . "\n";
				break;
			case 'application':
				//pgp signature
				if ( $part->ctype_secondary == 'pgp-signature' ) {break;}
				//other attachments
				$filename = $filesdir . $part->ctype_parameters['name'];
				$fp = fopen($filename, 'w');
				fwrite($fp, $part->body );
				fclose($fp);
				$meta_return .= '<a href="' . $filename . '">' . $part->ctype_parameters['name'] . '</a>' . "\n";
				break;
			}		
			return $meta_return;
		}

// This function turns Enriched Text into something similar to html
// Very basic at the moment, only supports some functionality and dumps the rest
function etf2html ( $content )
{

	$search = array(
		'/<bold>/',
		'/<\/bold>/',
		'/<underline>/',
		'/<\/underline>/',
		'/<italic>/',
		'/<\/italic>/',
		'/<fontfamily><param>.*<\/param>/',
		'/<\/fontfamily>/',
		'/<x-tad-bigger>/',
		'/<\/x-tad-bigger>/'
	);
	
	$replace = array (
		'<b>',
		'</b>',
		'<u>',
		'</u>',
		'<i>',
		'</i>',
		'',
		'',
		'',
		''
	);
		// strip extra line breaks
		$content = preg_replace($search,$replace,trim($content));
		return ($content);
}

// Keeps content until finds a line with '--'
// This effectively removes signatures
function removesig ( $content )
{
	$arrcontent = explode("\n", $content);
	$append = TRUE;
	$i = 0;
	for ($i = 0; $i<=count($arrcontent); $i++)
	{
		$line = $arrcontent[$i];
		$nextline = $arrcontent[$i+1];
		if ( preg_match('/^--$/',trim($line)) ) { $append = FALSE;}

		if (  $append == TRUE )
	    {
	//		if ($line != NULL && $nextline == NULL )
			//{ $strcontent .= $line ."</br>\n";;
			//check if there are null lines \n
			//} elseif ($line == NULL && $nextline == NULL) { $strcontent .= '</br>';
			//}else	{
				$strcontent .= $line;
		}
	}
	return $strcontent;
}

//filter content for new lines
function filternewlines ( $content )
{
		$search = array (
			'/ (\n|\r\n|\r)/',
			'/(\n|\r\n|\r)/'
		);
		$replace = array (
			' ',
			"\n"
		);
		// strip extra line breaks
		$return = preg_replace($search,$replace,$content);
		return $return;
}

// This function checks if the content is base64
function base64check ( $part )
{
//	if ($part->headers['content-transfer-encoding'] != 'base64')
//	{
		if ( strtolower($part->headers['content-transfer-encoding']) == 'base64' )
		{
			$part->body = base64_decode($part->body);
		}
//	}
	return $part;
}

function charsetcheck ( $part )
{
	if ( strtolower($part->ctype_parameters['charset'] == 'utf-8') )
	{
		$part->body = utf8_decode($part->body);
	}
	return $part;
}

// end of script
?>
